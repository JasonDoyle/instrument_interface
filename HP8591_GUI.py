import tkinter as tk
import HP8591E as hp

#TODO
#'GPIB0::18::INSTR'

class App(tk.Tk):
    def __init__(self):
        super().__init__()

        spectrum = hp.HP8591('GPIB0::18::INSTR')

        
        label_a = tk.Label(self, text="Trace A")
        clear_a = tk.Button(self, text="Clear Write", command=lambda: spectrum.clear_trace('A'))
        view_a = tk.Button(self, text="Veiw", command=lambda: spectrum.view_trace('A'))
        hold_max_a = tk.Button(self, text="Hold Max", command=lambda: spectrum.max_hold('A'))
        blank_a = tk.Button(self, text="Blank", command=lambda: spectrum.blank_trace('A'))
        #grab_a = tk.Button(self, text="Grab")
        save_a = tk.Button(self, text="Save", command=lambda: spectrum.save_csv('A'))

        label_b = tk.Label(self, text="Trace B")
        view_b = tk.Button(self, text="Veiw", command=lambda: spectrum.view_trace('B'))
        blank_b = tk.Button(self, text="Blank", command=lambda: spectrum.blank_trace('B'))
        hold_max_b = tk.Button(self, text="Hold Max", command=lambda: spectrum.max_hold('B'))
        clear_b = tk.Button(self, text="Clear Write", command=lambda: spectrum.clear_trace('B'))
        #grab_b = tk.Button(self, text="Grab")
        save_b = tk.Button(self, text="Save", command=lambda: spectrum.save_csv('B'))

        label_c = tk.Label(self, text="Trace C")
        view_c = tk.Button(self, text="Veiw", command=lambda: spectrum.view_trace('C'))
        blank_c = tk.Button(self, text="Blank", command=lambda: spectrum.blank_trace('C'))
        hold_min_c = tk.Button(self, text="Hold Min", command=lambda: spectrum.min_hold())
        clear_c = tk.Button(self, text="Clear Write", command=lambda: spectrum.clear_trace('C'))
        #grab_c = tk.Button(self, text="Grab")
        save_c = tk.Button(self, text="Save", command=lambda: spectrum.save_csv('C'))

        opts = { 'ipadx': 10, 'ipady': 10 , 'sticky': 'nswe' }
        label_a.grid(row=0, column=1, **opts)
        view_a.grid(row=0, column=2, **opts)
        blank_a.grid(row=0, column=3, **opts)
        hold_max_a.grid(row=0, column=4, **opts)
        clear_a.grid(row=0, column=5, **opts)
        #grab_a.grid(row=0, column=6, **opts)
        save_a.grid(row=0, column=7, **opts)

        opts = { 'ipadx': 10, 'ipady': 10 , 'sticky': 'nswe' }
        label_b.grid(row=1, column=1, **opts)
        view_b.grid(row=1, column=2, **opts)
        blank_b.grid(row=1, column=3, **opts)
        hold_max_b.grid(row=1, column=4, **opts)
        clear_b.grid(row=1, column=5, **opts)
        #grab_b.grid(row=1, column=6, **opts)
        save_b.grid(row=1, column=7, **opts)

        opts = { 'ipadx': 10, 'ipady': 10 , 'sticky': 'nswe' }
        label_c.grid(row=2, column=1, **opts)
        view_c.grid(row=2, column=2, **opts)
        blank_c.grid(row=2, column=3, **opts)
        hold_min_c.grid(row=2, column=4, **opts)
        clear_c.grid(row=2, column=5, **opts)
        #grab_c.grid(row=2, column=6, **opts)
        save_c.grid(row=2, column=7, **opts)


def main():
    #spectrum = hp.HP8591('GPIB0::18::INSTR')
    app = App()

    #model = spectrum.identity()
    #print("Model: {0}".format(model))
    #print("Units: {0}".format(spectrum.get_units()))
    #spectrum.view_trace('A')

    app.mainloop()
    
if __name__ == "__main__":
    main()

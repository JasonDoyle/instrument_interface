import visa
import matplotlib.pyplot as plt
import csv
import time

class HP8591:
    def __init__(self, address):
        #Create a resource manager
        resources = visa.ResourceManager()
        #Open the  instrument  by name. (Change this to the string for your instrument)
        self.instrument = resources.open_resource(address)

    def close(self):
        self.instrument.close()

    def identity(self):
        reading = self.instrument.query('ID?')
        reading = reading.split('\r\n')
        model = reading[0]
        return model

    def get_start_freq(self):
        return int(self.instrument.query('FA?'))
    
    def get_stop_freq(self):
        return int(self.instrument.query('FB?'))

    def get_units(self):
        return self.instrument.query('AUNITS?')

    def create_x_axis(self, trace):
        # creates x-axis for graphing
        start_freq = self.get_start_freq()
        stop_freq = self.get_stop_freq()

        step_size = int((stop_freq - start_freq)/(len(trace)))

        xaxis = list(range(start_freq, stop_freq, step_size))

        xaxis = xaxis[:len(trace)]
        
        return xaxis

    def csv_output(self, x_axis, y_axis):
        #file_name = 'plot'
        rows = zip(x_axis, y_axis)
        fieldnames = ['Frequency', 'Amplitude']
        with open('plot.csv', 'w', newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            for i in range(0,len(x_axis),1):
                writer.writerow({'Frequency': x_axis[i], 'Amplitude': y_axis[i]})

    def save_csv(self, trace_letter):
        filename = str(time.time()) + '_Trace' + trace_letter + '.csv'
        y_axis = self.save_trace(trace_letter)
        x_axis = self.create_x_axis(y_axis)

        rows = zip(x_axis, y_axis)
        fieldnames = ['Frequency', 'Amplitude']
        
        with open(filename, 'w', newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            for i in range(0,len(x_axis),1):
                writer.writerow({'Frequency': x_axis[i], 'Amplitude': y_axis[i]})
        

    def view_trace(self, trace_letter):
        trace = 'VIEW TR' + trace_letter
        self.instrument.write(trace)

    def blank_trace(self, trace_letter):
        trace = 'BLANK TR' + trace_letter
        self.instrument.write(trace)

    def clear_trace(self, trace_letter):
        trace = 'CLRW TR' + trace_letter
        self.instrument.write(trace)

    def max_hold(self, trace_letter):
        # Only works for Trace A and B
        trace = 'MXMH TR' + trace_letter
        self.instrument.write(trace)

    def min_hold(self):
        # Only works for Trace C
        self.instrument.write('MINH TRC')
        
    def save_trace(self, trace_letter):
        trace = []
        return_tr = []

        which_trace = 'T' + trace_letter
        trace = self.instrument.query(which_trace)
        trace = trace.split('\r\n')
        
        # Removes empty entries that cause errors when converting from string to float
        while '' in trace:
            trace.remove('')

        # Converts string numbers to floats
        for i in trace:
            i = float(i)
            return_tr.append(i)
        return return_tr
    


    def set_start_freq(self, freq):
        #TODO
        string = 'FA ' + str(freq)
        print(string)
        #instrument.write('FA freq')

    def set_stop_freq(self, freq):
        #TODO
        string = 'FB ' + str(freq)
        print(string)
        #instrument.write(string)

        
def main():

    #Create a resource manager
    ##resources = visa.ResourceManager()
    #Open the  instrument  by name. (Change this to the string for your instrument)
    ##spectrum = resources.open_resource('GPIB0::18::INSTR')

    spectrum = HP8591('GPIB0::18::INSTR')
    spectrum.timeout = 10000

    model = spectrum.identity()
    print("Model: {0}".format(model))
    print("Units: {0}".format(spectrum.get_units()))

    trace = spectrum.save_trace()

    x_axis = spectrum.create_x_axis(trace)

    spectrum.csv_output(x_axis, trace)
    
    plt.plot(x_axis, trace)
    #plt.title('{0}'.format(model))
    ##plt.ylim(-float(vscale)*5,float(vscale)*5)
    plt.minorticks_on()
    plt.grid(b=True, which='major')
    plt.show()
    spectrum.close()

if __name__ == '__main__':
    main()

#Bring in the VISA library
import visa
import serial
import matplotlib.pyplot as plt
import numpy as np

#Return the oscilloscope's ID string to tell us it's there
def identity(instrument):
    reading = instrument.query('*IDN?')
    reading = reading.split(',')
    Make = reading[0]
    Model = reading[1]
    return Make, Model


def main():

    #Create a resource manager
    resources = visa.ResourceManager('@py')
    #Open the  instrument  by name. (Change this to the string for your instrument)
    oscilloscope = resources.open_resource('ASRLCOM5::INSTR')
    oscilloscope.timeout = 25000

    Make, Model = identity(oscilloscope)
    print Make
    print Model

    #Select channel 1
    #oscilloscope.query(':MEAS:SOUR:CH1')
    #Read the RMS voltage on that channel
    #fullreading = oscilloscope.query(':MEAS:ITEM? VRMS,CH1')
    #Extract the reading from the resulting string...
    #readinglines = fullreading.splitlines()
    # ...and convert it to a floating point value.
    #reading = float(readinglines[0])
    #Send the reading to the terminal
    #print reading
    #Close the connection
    #oscilloscope.close()
    vscale = oscilloscope.query(':CH1:SCA?')

    print float(vscale)
    
    oscilloscope.write('DAT:STOP 2000')
    fullreading = oscilloscope.query(':CURV?')
    # Extract the reading from the resulting string...
    #readinglines = fullreading.splitlines()
    # ...and convert it to a floating point value.
    #reading = float(readinglines[0])
    #reading = readinglines[0]
    # Send the reading to the terminal
    #print fullreading
    # Close the connection
    oscilloscope.close()

    readinglines = fullreading.split(',')
    reading = map(float,readinglines)

    print type(reading[0])
    
    vrange = float(vscale) * 10
    scale = vrange/255

    print type(scale)
    
    scaled_data = [x*scale for x in reading]
    
    plt.plot(scaled_data)
    plt.title('{0} - {1}'.format(Make, Model))
    plt.ylim(-float(vscale)*5,float(vscale)*5)
    plt.minorticks_on()
    plt.grid(b=True, which='major')
    plt.show()

if __name__ == '__main__':
    main()
